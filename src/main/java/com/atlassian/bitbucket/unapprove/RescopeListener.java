package com.atlassian.bitbucket.unapprove;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.bitbucket.event.pull.PullRequestRescopedEvent;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestParticipant;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.Operation;
import com.google.common.collect.Iterables;

public class RescopeListener {

    private final AutoUnapproveSettings autoUnapproveSettings;
    private final PullRequestService pullRequestService;
    private final SecurityService securityService;
    private final TransactionTemplate txTemplate;

    public RescopeListener(AutoUnapproveSettings autoUnapproveSettings, PullRequestService pullRequestService,
                           SecurityService securityService, TransactionTemplate txTemplate) {
        this.autoUnapproveSettings = autoUnapproveSettings;
        this.pullRequestService = pullRequestService;
        this.securityService = securityService;
        this.txTemplate = txTemplate;
    }

    @EventListener
    public void onPullRequestRescoped(PullRequestRescopedEvent event) {
        final PullRequest pullRequest = event.getPullRequest();
        // only withdraw approval if the pull request's _source_ branch has changed, not it's target.
        if (!event.getPreviousFromHash().equals(pullRequest.getFromRef().getLatestCommit()) &&
                autoUnapproveSettings.isEnabled(pullRequest.getToRef().getRepository())) {
            txTemplate.execute(new TransactionCallback<Void>() {
                @Override
                public Void doInTransaction() {
                    for (PullRequestParticipant participant : Iterables.concat(pullRequest.getReviewers(), pullRequest.getParticipants())) {
                        // the withdrawApproval method withdraws approval for the currently authenticated user,
                        // so use SecurityService to impersonate the reviewer we are withdrawing approval for
                        securityService.impersonating(participant.getUser(), "Unapproving pull-request on behalf of user").call(new Operation<Object, RuntimeException>() {
                            @SuppressWarnings("ConstantConditions")
                            @Override
                            public Object perform() {
                                return pullRequestService.withdrawApproval(
                                        pullRequest.getToRef().getRepository().getId(),
                                        pullRequest.getId()
                                );
                            }
                        });
                    }
                    return null;
                }
            });
        }
    }
}
